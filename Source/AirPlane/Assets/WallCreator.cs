﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCreator : MonoBehaviour {

    public Object _wallPrefab;
    public float _distanceBetweenWalls = 18f;
    public float _wallLength = 16f;

    float _currentDistance = 0f;
    Vector3 _lastPosition;

	// Use this for initialization
	void Start () {
        _currentDistance = 0;
        _lastPosition = transform.position;

    }
	
	// Update is called once per frame
	void Update () {

        _currentDistance += (transform.position - _lastPosition).magnitude;

        if (_currentDistance > _distanceBetweenWalls)
        {
            Instantiate(_wallPrefab,this.transform.position - this.transform.forward * (_distanceBetweenWalls / 2f), transform.rotation);
            _currentDistance = 0;
        }

        _lastPosition = transform.position;
    }
}
